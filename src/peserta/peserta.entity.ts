import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity()
export class FileDataPeserta{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({length:30})
    nama_lengkap: string;

    @Column()
    asal: string;

    @Column()
    kelas: string;
    
}
