import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import { FileDataPeserta } from './peserta.entity';
import {Repository} from 'typeorm';
import { FilepesertaDTO } from './filepeserta.dto'
import { PesertaController } from './peserta.controller'

@Injectable()
export class PesertaService {
    constructor(
        @InjectRepository (FileDataPeserta)
        private filepesertaRepository: Repository<FileDataPeserta>
    ) {}

    async showAll(){
        return await this.filepesertaRepository.find();
    }

    async create(data: FilepesertaDTO){
        const filepesertaNew = await this.filepesertaRepository.create(data);
    await this.filepesertaRepository.save(filepesertaNew)
    return filepesertaNew
     }
}
