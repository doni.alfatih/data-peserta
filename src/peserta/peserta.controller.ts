import { Controller, Get, Post, Body } from '@nestjs/common';
import { PesertaService } from './peserta.service';
import { FilepesertaDTO } from './filepeserta.dto'

@Controller('peserta')
export class PesertaController {
    constructor(private FilePesertaService: PesertaService) {}

@Get()
lihatSemuaData(){
    return this.FilePesertaService.showAll();
}

@Post()
	membuatRecord(@Body() data: FilepesertaDTO){
	  return this.FilePesertaService.create(data)
	}

}
