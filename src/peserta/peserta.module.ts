import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileDataPeserta } from './peserta.entity';
import { PesertaController } from './peserta.controller';
import { PesertaService } from './peserta.service';

@Module({
    imports: [TypeOrmModule.forFeature([FileDataPeserta])],
    controllers: [PesertaController],
    providers: [PesertaService]
})
export class PesertaModule {}
