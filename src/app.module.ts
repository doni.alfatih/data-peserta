import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PesertaModule } from './peserta/peserta.module';
import { FileDataPeserta } from './peserta/peserta.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'db_peserta',
      entities: [FileDataPeserta],
      synchronize: true,
}),
    PesertaModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
